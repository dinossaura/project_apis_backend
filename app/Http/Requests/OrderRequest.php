<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'client_id' => 'required|numeric',
           'price' => 'required|numeric',
           'status' => 'required',    
        ];
    }

    public function messages()
    {
        return [
            'numeric' => 'Este campo aceita apenas números',
            'required' => 'Este campo deve ser preenchido',
        ];
    }
}
