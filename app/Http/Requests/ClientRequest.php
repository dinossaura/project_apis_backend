<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3|max:80',
            'last_name' => 'required|min:3|max:80',
            'email' => 'required|min:8',
        ];
    }

    public function messages()
    {
        return [
            'max' => 'Campo deve conter no máximo :max caracteres.',
            'min' => 'Campo deve ter no mínimo :min caracteres.',
            'required' => 'Este campo deve ser preenchido',
        ];
    }
}
