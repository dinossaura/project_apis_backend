<?php

namespace App\Http\Controllers\Api;

use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;    
    }

    public function index()
    {
        $orders = $this->order::all();

        return response()->json($orders, 200);
    }


    public function store(OrderRequest $request)
    {
        $data = $request->all();

        if(Client::find($data['client_id']))
        {
            try {
           
                $order = $this->order->create($data);
        
                   return response()->json([
                        'data' => [
                            'msg' => 'Pedido cadastrado com sucesso.'
                        ]   
                    ], 200);
        
                } catch (\Exception $e) {
        
                    return response()->jason(['error' => $e->getMessage()], 401);
        
                }

        } else {
            
            return response()->json([
                'data' => [
                    'msg' => 'Este cliente não consta na base de dados.'
                ]   
            ], 404); 

        }
                
        
    }

    public function show($id)
    {
        try {

           $order = $this->order->find($id);

           if($order){

            return response()->json([
                'data' => [
                    'order' => $order,
                    'msg' => 'Pedido retornado com sucesso.'
                ]   
            ], 200);

           }else {

               return response()->json([
                'data' => [
                    'msg' => 'Não foi possível retornar o pedido'
                ]   
            ], 404);

           }

           
        } catch (\Exception $e) {

            return response()->jason(['error' => $e->getMessage()], 401);

        }
    }

    public function update($id, OrderRequest $request)
    {
        $data = $request->all();
                
        try {

            $order = $this->order->find($id);



            if($order)
            {
                $order->update($data);

                return response()->json([
                     'data' => [
                         'msg' => 'Pedido alterado com sucesso.'
                     ]   
                 ], 200);

            } else {
 
                return response()->json([
                 'data' => [
                     'msg' => 'Não foi possível econtrar o pedido'
                 ]   
             ], 404);
 
            }

        } catch (\Exception $e) {

            return response()->jason(['error' => $e->getMessage()], 401);

        }
    }

    public function destroy($id)
    {
        try {

            $order = $this->order->find($id);

            if($order)
            {
                $order->delete();

                return response()->json([
                    'data' => [
                        'msg' => 'Pedido deletado com sucesso.'
                    ]   
                ], 200);

            } else {
 
                return response()->json([
                 'data' => [
                     'msg' => 'Não foi possível econtrar o pedido'
                 ]   
             ], 404);
 
            }

        } catch (\Exception $e) {

            return response()->jason(['error' => $e->getMessage()], 401);

        }
        
    }

    public function lastOrders()
    {
        $lastOrders = Order::take(5)->get();

        try {

            $lastOrders = Order::take(5)->get();

            return response()->json([
                'data' => [
                    'orders' => $lastOrders,
                ]   
            ], 200);

        } catch (\Exception $e) {

            return response()->jason(['error' => $e->getMessage()], 401);

        }
        
    }


}
