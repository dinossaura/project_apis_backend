<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id','price', 'status', 'created_at', 'updated_at'
    ];

    public function client()
    {
        return $this->hasOne(Client::class);
    }
}
